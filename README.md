## Simple Web API

Author: Mayreni Abajian


## Setup

This app was built using the Spring Boot framework and associated libraries.
Dependencies are managed in build.gradle.
You will need to run `gradle build` (either automated in your IDE or via command line if you have gradle installed) to import the dependencies into the project.

Once the app is compiled, you can run the server from Application.java.  The server will listen on localhost:8080

## Database

This app uses a mongoDB database. Upon starting, the app will try to connect to a local database server.  You can read about how to spin up a local instance of mongoDB [here](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/). 

The data is stored in two collections: Accounts and Contacts.  All documents have a unique ObjectId. Contacts may be linked to a specific account using the `accountId` field (which would correspond to the ObjectId of an existing Account)

## API

There are two controllers, one for Accounts data and one for Contacts.
Their respective paths are "/rest/accounts" and "/rest/contacts" where you can

- GET all objects ("/all")
- GET by ID ("/{id}")
- POST new object
- PUT updated object ("/{id}")
- and for contacts, also GET all contacts for a specific account ID ("rest/contacts/by/account/{accountId}")

## Testing

In Test folder, you will find some test cases with their expected output.

## TODO
- implement unit tests
- security framework
