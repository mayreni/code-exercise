package com.atlassian.codeexercise.model;

public class Address {
    private String streetAddr;
    private String streetAddrCont;
    private String city;
    private String state;
    private String zip;
    private String country;

    public String getStreetAddr() {
        return streetAddr;
    }

    public void setStreetAddr(String streetAddr) {
        this.streetAddr = streetAddr;
    }

    public String getStreetAddrCont() {
        return streetAddrCont;
    }

    public void setStreetAddrCont(String streetAddrCont) {
        this.streetAddrCont = streetAddrCont;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
