package com.atlassian.codeexercise.repository;

import com.atlassian.codeexercise.entity.Contact;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends MongoRepository<Contact,ObjectId> {

    public List<Contact> findAllByAccountId(ObjectId accountId);
}
