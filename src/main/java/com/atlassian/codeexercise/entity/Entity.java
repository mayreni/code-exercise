package com.atlassian.codeexercise.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Entity {
    @Id
    private ObjectId id = new ObjectId();

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }
}
