package com.atlassian.codeexercise.entity;

import com.atlassian.codeexercise.model.Address;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Account extends Entity {
    private String name;
    private Address address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
