package com.atlassian.codeexercise.rest;

import com.atlassian.codeexercise.entity.Contact;
import com.atlassian.codeexercise.repository.ContactRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("rest/contacts")
public class ContactController extends AbstractRestController<Contact>{

    @Autowired
    private ContactRepository contactRepository;

    @Override
    protected MongoRepository<Contact, ObjectId> repository() {
        return contactRepository;
    }

    @GetMapping("/by/account/{accountId}")
    public List<Contact> getAllByAccountId(@PathVariable ObjectId accountId) {
        return contactRepository.findAllByAccountId(accountId);
    }

}
