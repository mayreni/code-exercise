package com.atlassian.codeexercise.rest;

import com.atlassian.codeexercise.entity.Account;
import com.atlassian.codeexercise.entity.Contact;
import com.atlassian.codeexercise.repository.AccountRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("rest/accounts")
public class AccountController extends AbstractRestController<Account>{

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private ContactController contactController;

    @Override
    protected MongoRepository<Account, ObjectId> repository() {
        return accountRepository;
    }

    /** TODO: Decide whether to keep this endpoint or the one in ContactController. Only one needed.
     * @see ContactController#getAllByAccountId(ObjectId)
     */
    @GetMapping("/{id}/contacts")
    public List<Contact> getAllContactsForAccount(@PathVariable ObjectId id) {
        return contactController.getAllByAccountId(id);
    }
}
