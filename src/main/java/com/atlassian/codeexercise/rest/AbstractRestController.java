package com.atlassian.codeexercise.rest;

import com.atlassian.codeexercise.entity.Entity;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class AbstractRestController<T extends Entity> {

    protected abstract MongoRepository<T,ObjectId> repository();

    @GetMapping("/all")
    public List<T> getAll() {
        return repository().findAll();
    }

    @GetMapping("/{id}")
    public T get(@PathVariable ObjectId id) {
        return repository().findById(id).orElse(null);
    }

    @PostMapping("")
    public T create(@RequestBody T object) {
        if (repository().existsById(object.getId()))
            object.setId(new ObjectId()); // ensure idempotency (new resource created every time)
        return repository().save(object);
    }

    @PutMapping("/{id}")
    public T update(@PathVariable ObjectId id, @RequestBody T object) {
        object.setId(id);
        return repository().save(object);
    }

}
