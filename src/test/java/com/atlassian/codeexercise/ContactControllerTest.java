package com.atlassian.codeexercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ContactControllerTest {

	@Test
	void getAllObjects() {
		// should return all objects
	}
	@Test
	void getObjectById_Found() {
		// should return object
	}
	@Test
	void getObjectById_NotFound() {
		// should return null
	}
	@Test
	void createNewObject_ExistingId() {
		// should change id and insert as new
	}
	@Test
	void createNewObject_NewId() {
		// should insert as new
	}
	@Test
	void updateExistingObject() {
		// should update
	}
	@Test
	void updateExistingObject_MismatchedIds() {
		// the id from path should override the id in request body
	}
	@Test
	void updateNonExistingObject() {
		// should insert as new
	}
	@Test
	void getAllContactsWithAccountId() {
		// should return all contacts with matching account id
	}



}
